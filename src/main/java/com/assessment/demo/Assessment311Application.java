package com.assessment.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assessment311Application {

	public static void main(String[] args) {
		SpringApplication.run(Assessment311Application.class, args);
	}

}
