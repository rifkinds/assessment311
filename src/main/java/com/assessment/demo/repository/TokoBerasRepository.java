package com.assessment.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.assessment.demo.model.TokoBeras;

public interface TokoBerasRepository extends JpaRepository<TokoBeras, Long>{
	@Query(value = "SELECT * FROM toko_beras WHERE is_active = true", nativeQuery = true)
	List<TokoBeras> findByReady(Boolean is_active);
	
	@Query(value = "SELECT * FROM toko_beras WHERE LOWER(tipe_beras) LIKE LOWER (concat('%',?1,'%' )) AND is_active=true", nativeQuery=true)
	List<TokoBeras> findByKeyword(String name);
}
