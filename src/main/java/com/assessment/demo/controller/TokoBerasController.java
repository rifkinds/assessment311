package com.assessment.demo.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.assessment.demo.model.TokoBeras;
import com.assessment.demo.repository.TokoBerasRepository;

@RestController
@RequestMapping("/master/")
public class TokoBerasController {
	
	@Autowired
	TokoBerasRepository tokoBerasRepository;
	
	@GetMapping("tokoberas")
	public ResponseEntity<List<TokoBeras>> getAllTokoBeras(){
		try {
			List<TokoBeras> listTokoBeras = this.tokoBerasRepository.findByReady(true);
			return new ResponseEntity<>(listTokoBeras, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("tokoberas/add")
	public ResponseEntity<Object> tambahJenjang(@RequestBody TokoBeras tokoBeras){
		tokoBeras.setIsActive(true);
		tokoBeras.setCreateBy("admin1");
		tokoBeras.setCreateDate(new Date());
		
		TokoBeras TokoBerasData = this.tokoBerasRepository.save(tokoBeras);
		
		if(TokoBerasData.equals(tokoBeras)) {
			return new ResponseEntity<>("Save Data Success", HttpStatus.OK);
		}else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("tokoberas/{id}")
	public ResponseEntity<Object> getDataById(@PathVariable("id") Long id){
		try {
			Optional<TokoBeras> tokoBerasData = this.tokoBerasRepository.findById(id);
			return new ResponseEntity<>(tokoBerasData, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("tokoberas/edit/{id}")
	public ResponseEntity<Object> editTokoBeras(@PathVariable("id") Long id, @RequestBody TokoBeras tokoBeras){
		Optional<TokoBeras> tokoBerasData = this.tokoBerasRepository.findById(id);
		
		if(tokoBerasData.isPresent()) {
			tokoBeras.setId(id);
			tokoBeras.setModifyBy("admin2");
			tokoBeras.setModifyDate(new Date());
			tokoBeras.setCreateBy(tokoBerasData.get().getCreateBy());
			tokoBeras.setCreateDate(tokoBerasData.get().getCreateDate());
			this.tokoBerasRepository.save(tokoBeras);
			return new ResponseEntity<Object>("Updated Success", HttpStatus.OK);

		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("tokoberas/delete/{id}")
	public ResponseEntity<Object> deleteTokoBeras(@PathVariable("id") Long id){
		Optional<TokoBeras> tokoBerasData = this.tokoBerasRepository.findById(id);

		if(tokoBerasData.isPresent()) {
			TokoBeras tokoBeras = new TokoBeras();
			
			tokoBeras.setId(id);
			tokoBeras.setIsActive(false);
			tokoBeras.setHargaSekilo(tokoBerasData.get().getHargaSekilo());
			tokoBeras.setTipeBeras(tokoBerasData.get().getTipeBeras());
			tokoBeras.setModifyBy(tokoBerasData.get().getModifyBy());
			tokoBeras.setModifyDate(tokoBerasData.get().getModifyDate());
			tokoBeras.setCreateBy(tokoBerasData.get().getCreateBy());
			tokoBeras.setCreateDate(tokoBerasData.get().getCreateDate());
			this.tokoBerasRepository.save(tokoBeras);
			return new ResponseEntity<Object>("Updated Success", HttpStatus.OK);

		}else {
			return ResponseEntity.notFound().build();
		}	
	}
	
	@GetMapping("tokoberas/search")
	public ResponseEntity<List<TokoBeras>> searchByTipeBeras(@RequestParam("keyword") String keyword){
		List<TokoBeras> searchList = this.tokoBerasRepository.findByKeyword(keyword);
		
		return new ResponseEntity<>(searchList, HttpStatus.OK);
		
	}
}
