//package com.assessment.demo.model;
//
//import java.util.Date;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.OneToMany;
//import javax.persistence.Table;
//
//@Entity
//@Table(name = "guru")
//public class Guru {
//
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name = "id")
//	private Long id;
//	
//	@Column(name = "mata_pelajaran_id")
//	private String mataPelajaranId;
//	
//	@Column(name = "nama_guru")
//	private String namaGuru;
//	
//	@Column(name = "jenjang_pendidikan")
//	private String jenjangPendidikan;
//	
//	@Column(name = "is_active")
//	private Boolean isActive;
//	
//	@Column(name = "create_by")
//	private String createBy;
//	
//	@Column(name = "create_date")
//	private Date createDate;
//	
//	@Column(name = "modify_by")
//	private String modifyBy;
//	
//	@Column(name = "modify_date")
//	private Date modifyDate;
//	
//	@OneToMany
//	@JoinColumn(name = "mata_pelajaran_id", insertable = false, updatable=false)
//	private MataPelajaran mataPelajaran;
//
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public String getMataPelajaranId() {
//		return mataPelajaranId;
//	}
//
//	public void setMataPelajaranId(String mataPelajaranId) {
//		this.mataPelajaranId = mataPelajaranId;
//	}
//
//	public String getNamaGuru() {
//		return namaGuru;
//	}
//
//	public void setNamaGuru(String namaGuru) {
//		this.namaGuru = namaGuru;
//	}
//
//	public String getJenjangPendidikan() {
//		return jenjangPendidikan;
//	}
//
//	public void setJenjangPendidikan(String jenjangPendidikan) {
//		this.jenjangPendidikan = jenjangPendidikan;
//	}
//
//	public Boolean getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(Boolean isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getCreateBy() {
//		return createBy;
//	}
//
//	public void setCreateBy(String createBy) {
//		this.createBy = createBy;
//	}
//
//	public Date getCreateDate() {
//		return createDate;
//	}
//
//	public void setCreateDate(Date createDate) {
//		this.createDate = createDate;
//	}
//
//	public String getModifyBy() {
//		return modifyBy;
//	}
//
//	public void setModifyBy(String modifyBy) {
//		this.modifyBy = modifyBy;
//	}
//
//	public Date getModifyDate() {
//		return modifyDate;
//	}
//
//	public void setModifyDate(Date modifyDate) {
//		this.modifyDate = modifyDate;
//	}
//
//	public MataPelajaran getMataPelajaran() {
//		return mataPelajaran;
//	}
//
//	public void setMataPelajaran(MataPelajaran mataPelajaran) {
//		this.mataPelajaran = mataPelajaran;
//	}
//
//	
//}
