//package com.assessment.demo.model;
//
//import java.util.Date;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
//
//@Entity
//@Table(name = "nilai")
//public class Nilai {
//	
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name = "id")
//	private Long id;
//	
//	@Column(name = "grade")
//	private String grade;
//
//	@Column(name = "is_active")
//	private Boolean isActive;
//	
//	@Column(name = "create_by")
//	private String createBy;
//	
//	@Column(name = "create_date")
//	private Date createDate;
//	
//	@Column(name = "modify_by")
//	private String modifyBy;
//	
//	@Column(name = "modify_date")
//	private Date modifyDate;
//
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public String getGrade() {
//		return grade;
//	}
//
//	public void setGrade(String grade) {
//		this.grade = grade;
//	}
//
//	public Boolean getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(Boolean isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getCreateBy() {
//		return createBy;
//	}
//
//	public void setCreateBy(String createBy) {
//		this.createBy = createBy;
//	}
//
//	public Date getCreateDate() {
//		return createDate;
//	}
//
//	public void setCreateDate(Date createDate) {
//		this.createDate = createDate;
//	}
//
//	public String getModifyBy() {
//		return modifyBy;
//	}
//
//	public void setModifyBy(String modifyBy) {
//		this.modifyBy = modifyBy;
//	}
//
//	public Date getModifyDate() {
//		return modifyDate;
//	}
//
//	public void setModifyDate(Date modifyDate) {
//		this.modifyDate = modifyDate;
//	}
//}
