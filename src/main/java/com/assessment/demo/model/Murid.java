//package com.assessment.demo.model;
//
//import java.util.Date;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.OneToMany;
//import javax.persistence.Table;
//
//@Entity
//@Table(name = "murid")
//public class Murid {
//	
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name = "id")
//	private Long id;
//	
//	@Column(name = "mata_pelajaran_id")
//	private Long mataPelajaranId;
//	
//	@Column(name = "nama_murid")
//	private String namaMurid;
//	
//	@Column(name = "nilai_id")
//	private int nilaiId;
//	
//	@Column(name = "is_active")
//	private Boolean isActive;
//	
//	@Column(name = "create_by")
//	private String createBy;
//	
//	@Column(name = "create_date")
//	private Date createDate;
//	
//	@Column(name = "modify_by")
//	private String modifyBy;
//	
//	@Column(name = "modify_date")
//	private Date modifyDate;
//
//	@OneToMany
//	@JoinColumn(name="mata_pelajaran_id", insertable = false, updatable=false)
//	private MataPelajaran mataPelajaran;
//	
//	@OneToMany
//	@JoinColumn(name="nilai_id", insertable = false, updatable=false)
//	private Nilai nilai;
//
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public Long getMataPelajaranId() {
//		return mataPelajaranId;
//	}
//
//	public void setMataPelajaranId(Long mataPelajaranId) {
//		this.mataPelajaranId = mataPelajaranId;
//	}
//
//	public String getNamaMurid() {
//		return namaMurid;
//	}
//
//	public void setNamaMurid(String namaMurid) {
//		this.namaMurid = namaMurid;
//	}
//
//	public int getNilaiId() {
//		return nilaiId;
//	}
//
//	public void setNilaiId(int nilaiId) {
//		this.nilaiId = nilaiId;
//	}
//
//	public Boolean getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(Boolean isActive) {
//		this.isActive = isActive;
//	}
//
//	public String getCreateBy() {
//		return createBy;
//	}
//
//	public void setCreateBy(String createBy) {
//		this.createBy = createBy;
//	}
//
//	public Date getCreateDate() {
//		return createDate;
//	}
//
//	public void setCreateDate(Date createDate) {
//		this.createDate = createDate;
//	}
//
//	public String getModifyBy() {
//		return modifyBy;
//	}
//
//	public void setModifyBy(String modifyBy) {
//		this.modifyBy = modifyBy;
//	}
//
//	public Date getModifyDate() {
//		return modifyDate;
//	}
//
//	public void setModifyDate(Date modifyDate) {
//		this.modifyDate = modifyDate;
//	}
//
//	public MataPelajaran getMataPelajaran() {
//		return mataPelajaran;
//	}
//
//	public void setMataPelajaran(MataPelajaran mataPelajaran) {
//		this.mataPelajaran = mataPelajaran;
//	}
//
//	public Nilai getNilai() {
//		return nilai;
//	}
//
//	public void setNilai(Nilai nilai) {
//		this.nilai = nilai;
//	}
//	
//}
